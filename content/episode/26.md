+++
title = "سيف العطار يكلمنا عن الـ Continuous Integration التوحيد المستمر للمصدر"
audio_file = "DotNetArabi_026_SeifAttar-CI"
date = 2012-06-12T00:00:00Z
audio_length = "53"
guests = ["seifattar"]
number = 26
+++
يكلمنا سيف العطار عن الـ Continuous Integration أي التوحيد المستمر للمصدر: ما هو، ما هي مكوناته الأساسية، أهم الأدوات المستخدمة في هذا المجال و الطرق الممكن استخدامها فيه.
