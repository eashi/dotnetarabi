+++
Title = "محمد زايد"
image = "mohammadzayedpersonal.jpg"
+++
محمد زايد، أمضى عدة سنوات في مجال برمجة الويب (الانترنت) و الـ windows applications و الـ Mobile applications ثم تمركز تخصصه في مجال الـ Sharepoint، التحق بمايكروسفت و هو بعمر 22 سنة، و يعمل حاليا  كمتخصص تقنيات استراتيجي لكبار العملاء في مايكروسوفت الأردن. و يمكن التواصل معه على البريد mzayed/microsoft.com
