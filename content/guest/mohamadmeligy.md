+++
Title = "محمد مليجي"
image = "mohamadmeligy.jpg"
+++
<p>محمد مليجي: مبرمج محترف  تخصص في تطبيفات الويب web ، و برامج إدارة المحتوى CMS (Content Management Systems)، الـ Custom Workflows ، Portals البوابات، ePayment Gateways بوابات الدفع من خلال الإنترنت، الـ Service Oriented Architecture التطبيقات القائمة على الخدمات. عمل لدى مؤسسات حكومية، و خاصة، و خيرية في كل من مصر و السعودية و أذربيجان، و الولايات المتحدة الأمريكية، و يعمل حاليا في Sydney في شركة Readify للاستشارات البرمجية</p> 
<p>كما عرف عنه نشاطه في مجتمع المبرمجين في مصر، فهو متحدث معروف، و ألقى العديد من المحاضرات، عن الـ Asp.net Ajax، و اللغات التفاعلية Dynamic Languages، foundation windows Workflow، و عن، Search Engine Optimization أي تحسين فرص الموقع في الظهور على الصفحات الأولى لمحركات البحث، و الـ  Agile Processes Scrum و هي نوع جديد من إدارة تطوير البرمجيات، عن الـ Design patterns الأنماط المتكررة في تصميم الرمجيات  و عن العديد من التكنولوجيا الحديثة.<br>
يمكن التواصل معه عبر مدونته <a href=" http://gurustop.net">http://gurustop.net</a>  و تويتر <a href="http://twitter.com/Meligy">@Meligy</a></p>

