+++
Title = "محمد حلبي"
image = "mohamadhalabi.jpg"
+++
<p>محمد حلبي حاصل على شهادة البكالوريوس في هندسة الكمبيوتر، و على العديد من الشهادات مثل IBM OOAD، و الـ IASA، و الـ TOGAF, و الـ IEEE، و الـ MCSD، و الـ ITIL، و الـ PMP.</p>
<p>يعمل محمد حاليا كمهندس تصميم البرمجيات Solution Architect و مجال عمله يتمحور حول تصميم حلول التكامل عن طريق أدوات مثل BizTalk Server، و الـ WCF، و الـ WF، و الـ ADFS، و الـ WIF، بالإضافة إلى الأنماط الهندسية Architectural Patterns مثل الـ ESB، و الـ SOA. بالإضافة إلى إدارة دورة حياة النظم الـ Application Life-cycle Management  من خلال تصميم المسارات و أتمتتها عن طريق الـ Team Foundation Server.</p>
<p>محمد هو Microsoft Integration MVP، و قبل ذلك كان ASP.NET/IIS MVP، و هو "قائد مجتمع مايكروسوفت" Microsoft Community Lead، و متحدث في كثير من الفعاليات مثل TechEd، و الـ Gulf Developer Conference، و الـ Open Door، بالإضافة إلى العديد من الفعاليات المحلية.</p>

