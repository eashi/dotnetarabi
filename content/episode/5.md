+++
title = "محمد مليجي يتكلم عن الـ ORM (Object Relational Mapping)"
audio_file = "DotnetArabi_005_MohamadMeligy"
date = 2009-08-27T00:00:00Z
audio_length = "61"
guests = ["mohamadmeligy"]
number = 5
+++
محمد مليجي تكلم عن الـ ORM (Object Relational Mapping) و هي برامج مساعدة تستطيع من خلالها نقل المعلومات و تحويلها من طبيعة قاعدة البيانات إلى طبيعة البرامج المبنية بأسلوب الـ Object Oriented. حلقة غنية بالتفاصيل و المعلومات القيمة جدا.
