+++
Title = "معتصم العوا"
image = "moutasemalawa.jpg"
+++
معتصم العوا، معماري تقنى Technical Architect لديه من الخبرة 8 سنوات عمل فيها في عدة مؤسسات على تقنية الـ SharePoint و الـ Microsoft Dynamics. و هو عضو نشط في المجتمع التقني، و تكلم في عدة لقاءات تنقية مثل الـ SharePoint Saturday في الكويت. و هو يعمل حاليا كمعماري تقني Technical Architect في شركة إيبكس eBECS في بريطانيا. يمكن التواصل مع معتصم عبر تويتر <a href="http://twitter.com/moutasema">@moutasema</a>
