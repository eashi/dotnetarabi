+++
title = "تغطية فعاليات شيربوينت ساترداي ستيودنتس إدشن"
audio_file = "DotNetArabi_023_SPA_students"
date = 2011-10-01T00:00:00Z
audio_length = "35"
guests = ["spsjordan"]
number = 23
+++
<p>ضمن فعاليات شيربوينت ساترداي المشهورة <a href="http://www.sharepointsaturday.org/jordan/default.aspx"> SharePoint Saturday </a> قام مجتمع تقنيي شيربوينت في الأردن JSUG بإقامة يوم خاص للطلاب، حيث وجه محتوى المحاضرات لمستوى طلاب الجامعات، فنوقشت مواضيع مختلفة تؤسس الطلاب لمستوى يختزل على الطالب جهدا كبيرا للانتقال لمرحلة متقدمة في الشيربوينت.</p>  <p>غطت الفعالية مواضيع مثل: "ما هو الشيربوينت "What is SharePoint"، و "فهم أساسيات الشيربوينت" "Understanding SharePoint pre-requisites"، و "عمليات البحث في الشيربوينت" "Search in SharePoint" و "بناء مسارات قرارت العمل" "Developing workflows in SharePoint" و غيرها. </p>  تحدث في هذه الفعالية كلا من:<ul>  <li>علاء عجوة</li>  <li>علي نمر</li>  <li>حكمت كنعان</li>  <li>محمد الدرهلي</li>  <li>محمد وردات</li>  <li>رائد طه</li>  <li>رند خلف</li>  <li>سائد شلا</li>  <li>زكي شاهين</li>  </ul>
