+++
Title = "تيسير جودة"
image = "taiseerjoudeh.jpg"
+++
<p>تيسير جودة، لديه ثمان سنوات من الخبرة، قضى معظمها في تقنية الدوت نت، امتدت لعدة تقنيات مثل ASP.NET, WCF, Windows Mobile. تم تطبيقها في أنظمة حلول المواقع Geographical Location Services و حلول الـ CRM و الـ e-Commerce.</p>
<p>حصل تيسير على شهادة البكالوريوس في علم الحاسوب من الجامعة الأردنية و الماجستير في إدارة المشاريع التقنية من جامعة ديبول. و هو حاصل على شهادة الـ Srum Master و الـ MCAD. و هو يعمل حاليا كقائد فريق Senior Technical Team Leader في شركة أرامكس.</p>
<p>تركز اهتمام تيسير مؤخرا على بناء المواقع بأسلوب الـ Single Page Applications ، و front end development أي البرمجة المتركزة على الواجهات التي يتعامل معها المستخدم الأخير.</p>
<p>يمكن التواصل معه من خلال موقعه الذي يدون فيه باستمرار على العنوان <a href="http://bitoftech.net">bitoftech.net</a> و حسابه على تويتر <a href="http://twitter.com/tjoudeh">@tjoudeh</a></p>
