+++
title = "عبدالهادي بو خريس يتكلم عن الـ HTML5"
audio_file = "DotNetArabi_017_AbduBurkes-HTML5"
date = 2011-03-26T00:00:00Z
audio_length = "56"
guests = ["abdu"]
number = 17
+++
يتكلم عبدالهادي عن الـ HTML5 و عن أهم خصاصئها الجديدة، إضافة إلى أهم الأدوات التي تساعد المبرمجين على استخدامها بالنسبة للمتصفحات المختلفة. كما تكلم أيضا عن علاقتها مع الـ Flash و Silverlight.
