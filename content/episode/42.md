+++
title = "حسام بركات يتكلم عن توجه \"الخدمات المُدارة\" السيرفرليس Serverless"
audio_file = "DotNetArabi_042_HossamBarakat-Serverless"
date = 2018-03-05T00:00:00Z
audio_length = "52"
guests = ["hossambarakat"]
number = 42
+++
تكلم حسام بركات عن توجه السيرفرليس Serverless المتواجد بقوة على الساحة التقنية. تكلم عن ماهيتها، و كيف يمكن للمطورين العمل معها. 
