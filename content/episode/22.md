+++
title = "محمد موسى يتكلم عن نمط الـ MVP في الـ ASP.NET"
audio_file = "DotNetArabi_022_MohammedMousa-ASPNET-MVP"
date = 2011-09-17T00:00:00Z
audio_length = "45"
guests = ["muhammadmousa"]
number = 22
+++
تكلم محمد موسى عن النمط البرمجي المسمى بالـ MVP، تكلم عن هيئته، و عن حسناته، و عن الصعوبات التي قد تواجه فريق العمل عند استخدامه. كما تكلم عن الفرق بينه و بين نمط الـ MVC و عن أفضل استخدام لكل واحد منهما.
