+++
Title = "أيمن عفانة"
image = "aymanafanah.jpg"
+++
<p>أيمن عفانة، حاصل على درجتي البكالوريوس و الماجستير في الهندسة من جامعة McGill في  تخرج منها بامتياز و ضمن الطلاب العشر الأوائل ، و هو حاصل على شهادة إدارة البرمجيات كمنتج أي Software Product Management من جامعة Washington.</p>
<p>عمل أيمن كمدير تسويق في شركة Infotek consulting services و من ثم انتقل إلى شركة Microsoft ليعمل مع فريق Windows Vista، و فريق Windows server 2008، و فريق  Windows 7 كمهندس برمجيات و مسؤول جودة لمدة ثلاث سنوات، عمل خلالها في عدة جوانب، منها جانب الـ Performance Monitor و Task Manager و Resource Monitor.
ثم انتقل لفريق Windows Phone كمدير برنامج أي Program Manager لمدة 4 سنوات و نصف، ليكون مسؤولا عن العديد من جوانب الـ Windows Phone منها: <ul>
<li>جانب الشبكات أي الـ Networking: مثل الـ Sockets، و الـ HTTP، و الـ Authentication،  الـ WCF
<li>جانب الوسائط المتعددة أي Media: مثل التدفق المتواصل أي الـ Streaming و الـ Playback، و الـكاميرا، 
<li>جانب البناء التطبيقي أي الـ Application Model: مثل الـ App Experience، و نموذج التصفح Navigation Model مثل الانتقال من برنامج لبرنامج، 
و الكثير غيرها...</ul>
<p>و في عام 2012 عاد أيمن إلى الأردن ليستمر في العمل مع Microsoft  و لكن كـمدير برنامج التوعية لنظام التطوير التقني Developer Platform Evangelist Lead </p>

