+++
Title = "فارس صويص"
image = "farissweis.jpg"
+++
<a href="http://twitter.com/farissweis" >فارس صويص </a>، مهندس التصميم الأول Chief Software Architect  في شركة <a href="http://www.telerik.com/">  Telerik  </a>المشهورة، عمل في مجال ضمانة جودة البرمجيات Software Quality Assurance لمدة تزيد عن 12 سنة، 9 منها في Microsoft في مجال بناء الحلول المأتمتة لضمان جودة البرمجيات Test Automation.
