+++
Title = "محمد سالم"
image = "salempersonal.jpg"
+++
محمد سالم، مبرمج متقدم ذو خمس سنوات من الخبرة، قضى اثنتين منها في "معالجة الصور Image processing" باستخدام لغة الـ C++، ثم كان تركيزه في الثلاث السنوات الأخيرة على الـ"دوت نت .net" من خلال العمل على برامج مالية و اقتصادية كبيرة.<br> كما و لديه نشاط كبير في "جوردف Jordev"؛ مجتمع مطوري الـ"دوت نت .net" في الأردن و خصوصا القاء المحاضرات.  و لديه مدونة على العنوان <span dir="ltr"><a href="http://geeksconnected.com/msalem">http://geeksconnected.com/msalem</a></span> . و يمكن الاتصال به على البريد mohammad.waleed/gmail.com
