+++
Title = "خالد حنيدق"
image = "khaledhenidak.jpg"
+++
خالد حنيدق مبرمج مصري يعمل حالياً في مايكروسوفت. خالد لديه 20 سنة خبرة في بناء حلول تقنية، و خاصة في بناء  "solutions hyperscale" - أي الحلول التي لديها القدرة على خدمة عدد هائل من المستخدمين -. يمكن التواصل مع خالد عبر تويتر <a href="http://twitter.com/khnidk">@khnidk</a> أو عبر <a href="http://github.com/khenidak">github/khenidak</a>. 

