+++
Title = "وسيم الأطرش"
image = "wasimalatrash.jpg"
+++
<p>حاصل على شهادة هندسة أنظمة الحاسوب من جامعة الأزهر في غزة سنة 2012، و حاصل على المركز الأول في مسابقة Startup Weekend Gaza 2012 ، و حاصل على المركز الثاني في مسابقة IT Open Day على مستوى جامعات غزة.</p>
يكتب على مدونته <a href="http://dotwasim.com">dotwasim.com</a> و يمكن متابعته على تويتر على <a href="http://twitter.com/dotwasim">@dotwasim</a>.
