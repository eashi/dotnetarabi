+++
Title = "ماهر الجندوبي"
image = "maherjendoubi.jpg"
+++
<p><a href="http://www.maherjendoubi.io/">ماهر الجندوبي</a> مهندس برمجيات مختص في مجال الدوت نت، لديه 9 سنوات من الخبرة قضى معظمها في إنشاء برامج المؤسسات أو بما يعرف بـ Line of Business Applications.</p><p>اختص ماهر في مجال الـ #C و الـ XAML في مستهل حياته المهنية، ثم ركز على الـ ASP.NET Core  منذ نشأته و النظام السحابي Microsoft Azure، لديه عدة مقالات في هذا المجال على مدونته، و العديد من المشاركات في منتدى الـ ASP.NET Core، حيث حصل على جائزة الـ MVP Microsoft Valuable Professional.</p><p>يعمل حاليا في مستشفى Gustave Roussy في فرنسا، و يمكن التواصل معه على تويتر <a href="http://twitter.com/maherjend">@maherjend</a>.
</p>
