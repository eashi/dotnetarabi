+++
Title = "مروان طارق"
image = "marwantarek.jpg"
+++
مروان طارق: تقني محترف في مجال  "الشيربوينت" Sharepoint بدأ معه منذ 2005 و عمل على جميع إصداراته 2003, 2007 and 2010. و هو يحمل عدة شهادات من "مايكروسوفت" Microsoft منها MCAD, MCSD, MCPD, MCTS كلها في مجال الـ Sharepoint. 
كما أنه مؤسس مجموعة مطوري Sharepiont  مصر (Egypt Sharepoint User Group) و هو مؤسس مشارك لموقع <a href="http://www.SharePoint4Arabs.com"> SharePoint4Arabs.com</a> أول موقع عربي ينشر محتوى معلوماتي من خلال الفيديو باللغة العربية عن الـ Sharepoint. و أيضا هو محاضر في كثير من الفعاليات و الأنشطة مثل Microsoft Innvoation day, Sharepoint Saturday,   و المحاضرات التي تنظمها مجموعة Egypt Sharepoint User Group.
يعمل مروان حاليا لدى شركة مايكروسوفت كتقني محترف لحلول الـ Sharepoint، و يمكنكم التواصل معه من خلال مدونته <a href="http://www.marwantarek.com"> www.marwantarek.com</a>  و على تويتر @marwantarek.

