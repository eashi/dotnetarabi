+++
Title = "سيف العطار"
image = "seifattar.jpg"
+++
<a href="https://twitter.com/#!/seifattar">سيف العطار</a>، مبرمج ذو خبرة واسعة تمتد على مدى عشر سنوات قضاها بتصميم و تطبيق البرمجيات ابتداء من Content Management  أي إدارة المحتوى  مرورا بـ Server Management for media streaming  أي إدارة خوادم بث المحتوى المرئي و الصوتي و DNS Management Software أي تطبيقات إدارة الـ DNS Domain Name Servers خوادم أسماء النطاق و e-commerce أي التجارة الإلكترونية. و يعتبر سيف من أشد المتمسكين بمبادئ الـ Agile و كتابة الشيفرة بشكل مرتب و خال من الأخطاء النمطية.</br> 
يعيش سيف حاليا في لندن ، و يعمل على بناء خدمات البنية التحتية البرمجية أي الـ Backend services لصالح موقع wiggle.co.uk  باستخدام نمط  التصميم SOA  وتقنية MSMQ.</br>
ساهم سيف العطار في العديد من البرامج مفتوحة المصدر، و هو يقود حاليا مشروع الـ S#arp Architecture المشهور.

