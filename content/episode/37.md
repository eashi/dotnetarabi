+++
title = "معتصم العوا يتكلم عن مايكروسوفت دايناميكس Microsoft Dynamics"
audio_file = "DotNetArabi_037_MoutasemAlAwa-MicrosoftDynamics"
date = 2016-10-31T00:00:00Z
audio_length = "39"
guests = ["moutasemalawa"]
number = 37
+++
تكلم معتصم العوا عن مايكروسوفت داينامكس، ما هو، و ما أهم مكوناته، و الأدوات المستخدمة لبنائه، و مميزاته، و سلبياته، و المستقبل مثل هذه البرامج.، 
